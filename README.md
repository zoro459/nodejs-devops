## Note

This project showcases my ability to efficiently deal with Node.js projects in Jenkins CI, demonstrating my expertise in implementing continuous integration workflows for Node.js applications in best practices.

# Node.js Jenkins CI Pipeline - Continuous Integration (CI)

Welcome to the Node.js Jenkins CI project! This repository showcases a Continuous Integration (CI) pipeline for a Node.js application, integrating various DevOps tools and best practices in the software development life cycle.
!['devops photo'](https://gitlab.com/zoro459/nodejs-devops/-/raw/main/Images/DEVOPS.jpg)
## Project Overview

This project demonstrates how to implement a CI pipeline for a Node.js application using Jenkins. The pipeline includes stages for versioning the app and Docker image, testing, downloading dependencies, building and pushing Docker images, and committing new changes to GitLab.

## Key Features

✅ Jenkins CI Pipeline: The pipeline is divided into multiple stages to automate the entire CI workflow.

✅ Node.js and NPM: The project uses Node.js and NPM to manage dependencies and run the application.

✅ Docker Integration: The pipeline builds Docker images of the Node.js application and pushes them to Docker Hub for easy deployment.

✅ Shared Library: The Jenkins pipeline utilizes a shared library that you created to promote reusability and maintainability.

## CI Pipeline

The Jenkins CI pipeline comprises the following stages:

1. **Versioning:** The pipeline increments the app version in the package.json file and sets a new Docker image tag.

2. **Testing and Dependency Download:** The application undergoes unit testing, and the necessary NPM dependencies are downloaded.

3. **Build and Push Docker Image:** The Node.js application is containerized in a Docker image, and the image is pushed to Docker Hub.

4. **Commit New Changes:** After a successful build and Docker image push, the pipeline commits the new version update to the GitLab repository.

## Shared Library

This pipeline uses a shared library created by me to promote best practices and maintainability. The shared library's repository can be found at:

[Link to Jenkins Shared Library Repository](https://gitlab.com/zoro459/jenkins-shared-library)

## Webhook Configuration

To enable automated pipeline triggering, configure a GitLab webhook to trigger the pipeline on new commits or changes.

## Requirements

Contributors to this Node.js Jenkins CI project should have a good understanding of:

- Software Development Life Cycle (SDLC)

- Node.js and NPM

- Jenkins and Groovy scripting

- Declarative Jenkins syntax

## Contribution and Feedback

I welcome any feedback, suggestions, or contributions from the community. Feel free to open an issue or submit a pull request.

Let's build and improve together! Happy CI! 🚀
