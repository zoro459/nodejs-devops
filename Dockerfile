FROM node:13-alpine
RUN mkdir /opt/app 
COPY ./app/* /opt/app/
WORKDIR /opt/app
RUN npm install
CMD node server.js
